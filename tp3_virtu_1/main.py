from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from pymongo import MongoClient
from bson import ObjectId  # Update to correct import

app = FastAPI()

# MongoDB connection
client = MongoClient("mongodb://imageMongo:27020/?directConnection=true&serverSelectionTimeoutMS=2000&appName=mongosh+2.0.2")
db = client["bdd_mongo"]
collection = db["products"]  # Renamed the collection to handle clients

# Pydantic model for client
class Client(BaseModel):
    first_name: str
    last_name: str
    email: str
    num_orders: int

# Endpoints for clients

@app.post("/add_client")
def add_client(client_data: Client):
    result = collection.insert_one(client_data.dict())
    return JSONResponse(content={"message": "Client added successfully", "id": str(result.inserted_id)})

@app.put("/update_client/{client_id}")
def update_client(client_id: str, client_data: Client):
    client_id = ObjectId(client_id)
    result = collection.update_one({"_id": client_id}, {"$set": client_data.dict()})
    if result.modified_count == 0:
        raise HTTPException(status_code=404, detail="Client not found")
    return JSONResponse(content={"message": "Client updated successfully"})

@app.delete("/delete_client/{client_id}")
def delete_client(client_id: str):
    client_id = ObjectId(client_id)
    result = collection.delete_one({"_id": client_id})
    if result.deleted_count == 0:
        raise HTTPException(status_code=404, detail="Client not found")
    return JSONResponse(content={"message": "Client deleted successfully"})

@app.get("/get_client/{client_id}")
def get_client(client_id: str):
    client = collection.find_one({"_id": ObjectId(client_id)})

    if client is None:
        raise HTTPException(status_code=404, detail="Client not found")
    return client

# Additional root endpoint
@app.get("/")
async def root():
    return {"message": "Hello World"}
